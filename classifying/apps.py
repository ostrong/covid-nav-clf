from django.apps import AppConfig


class ClassifyingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'classifying'
