from email.policy import default
from random import choices
from django.db import models
from django.forms import BooleanField
from django.utils.translation import gettext_lazy as _
from multiselectfield import MultiSelectField

# Create your models here.
class Record(models.Model):

    SUBJECT = (
        ("Diagnosis", "Diagnosis"),
        ("Diagnostics", "Diagnostics"),
        ("Co-morbidities", "Co-morbidities"),
        ("Care Management", "Care Management"),
        ("Special Populations", "Special Populations"),
        ("Therapuetics and Prevention", "Therapuetics and Prevention"),
        ("Resource Management", "Resource Management"),
        ("Logistics", "Logistics"),
        ("Infection Control", "Infection Control"),
        ("Public Health and Case Reporting", "Public Health and Case Reporting"),
        ("Epidemiology", "Epidemiology"),
        ("Clinical Epidemiology", "Clinical Epidemiology"),
        ("Reaserch", "Reaserch"),
        ("Ethics", "Ethics"),
        ("animal study", "animal study"),
    )

    ASSETTTYPE = (
        ("Data Visualization", "Data Visualization"),
        ("data collection", "data collection"),
        ("directory/list", "directory/list"),
        ("knowledge resource", "knowledge resource"),
        ("policies and procedures", "policies and procedures"),
        ("knowledge assertion", "knowledge assertion"),
        ("guidelines", "guidelines"),
        ("clinical decision support", "clinical decision support"),
        ("workflows", "workflows"),
        ("formal models", "formal models"),
        ("recommendation", "recommendation"),
        ("systematic review", "systematic review"),
    )

    CARESETTING = (
        ("home", "home"),
        ("telemedicine", "telemedicine"),
        ("ambulatory", "ambulatory"),
        ("acute care", "acute care"),
        (
            "laboratory radiology other environments",
            "laboratory radiology other environments",
        ),
        ("long term care", "long term care"),
        ("post acute care", "post acute care"),
        ("school", "school"),
    )

    CAREPROCESS = (
        ("prevention", "prevention"),
        ("screening", "screening"),
        ("diagnosis", "diagnosis"),
        ("treament", "treament"),
        ("follow up", "follow up"),
        ("clinical trial", "clinical trial"),
    )

    ep_id = models.CharField(max_length=100, null=True)
    title = models.CharField(max_length=10000, null=True)
    abstract = models.CharField(max_length=100000, null=True)
    subject = MultiSelectField(choices=SUBJECT, null=True, blank=True)
    assett_type = MultiSelectField(choices=ASSETTTYPE, null=True, blank=True)
    care_setting = MultiSelectField(choices=CARESETTING, null=True, blank=True)
    care_process = MultiSelectField(choices=CAREPROCESS, null=True, blank=True)
    is_modded = models.BooleanField(default=False)
