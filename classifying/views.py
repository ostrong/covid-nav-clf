from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from classifying.models import Record
from django.contrib.auth.mixins import LoginRequiredMixin

from functools import wraps


def login_required(f):
    @wraps(f)
    def g(request, *args, **kwargs):
        if request.user.is_authenticated:
            return f(request, *args, **kwargs)
        else:
            return redirect("/accounts/login")

    return g


# Create your views here.
class RecordUpdateView(LoginRequiredMixin, UpdateView):
    model = Record
    fields = ("assett_type", "care_setting", "care_process", "subject")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        r = self.get_object()
        print(r.abstract)
        context["subject"] = r.title
        context["abstract"] = r.abstract
        return context

    def form_valid(self, form, *args, **kwargs):
        form = form.save()
        form.is_modded = True
        form.save()
        return redirect(
            f"/record/{Record.objects.filter(is_modded=False).order_by('?').first().id}"
        )


@login_required
def redirect_for_dummies(request):
    return redirect(
        f"/record/{Record.objects.filter(is_modded=False).order_by('?').first().id}"
    )
