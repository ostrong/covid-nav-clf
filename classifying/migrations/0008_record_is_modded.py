# Generated by Django 4.0.4 on 2022-05-18 01:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('classifying', '0007_remove_record_is_modded'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='is_modded',
            field=models.BooleanField(default=False),
        ),
    ]
